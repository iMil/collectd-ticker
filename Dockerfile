FROM debian:stretch-slim

LABEL maintainer="Emile `iMil' Heitor <imil@home.imil.net>"

# backports needed for golang 1.9+
# did not use debian:backports because of the number of installed packages
RUN echo "deb http://deb.debian.org/debian stretch-backports main" >> \
    /etc/apt/sources.list \
    && apt-get -y update && apt-get -y -t stretch-backports \
    --no-install-recommends install collectd collectd-dev git golang \
    build-essential ca-certificates libprotobuf-c1 libmicrohttpd12

ENV GOPATH /go

WORKDIR ${GOPATH}

RUN go get collectd.org/api && \
    git clone https://github.com/iMilnb/collectd-ticker.git && \
    cd collectd-ticker && make && make install

WORKDIR /

RUN rm -rf ${GOPATH} \
    && apt-get -y purge collectd-dev git golang build-essential \
    && apt-get -y autoremove \
    && rm -rf /var/lib/apt/lists/*

COPY conf/* /etc/collectd/

ENTRYPOINT ["/usr/sbin/collectd", "-f"]
